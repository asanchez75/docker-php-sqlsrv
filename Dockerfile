FROM ubuntu:16.04

MAINTAINER Gildásio Júnior (gjuniioor@protonmail.com)

# update package list
RUN apt-get update

# install curl and git
RUN apt-get install -y curl git

# install apache
RUN apt-get install -y apache2

# install php
RUN apt-get -y install php7.0 mcrypt php7.0-mcrypt php-mbstring php7.0-mysql php-pear php7.0-dev php7.0-xml php7.0-gd php7.0-opcache php7.0-curl
RUN apt-get install -y libapache2-mod-php7.0 mysql-client

# install pre requisites
RUN apt-get update
RUN apt-get install -y apt-transport-https
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql mssql-tools
RUN apt-get install -y unixodbc-utf16
RUN apt-get install -y unixodbc-dev-utf16

#enabling pdo_mysql
RUN phpenmod pdo_mysql

# install driver sqlsrv
RUN pecl install sqlsrv
RUN echo "extension=sqlsrv.so" >> /etc/php/7.0/apache2/php.ini
RUN pecl install pdo_sqlsrv
RUN echo "extension=pdo_sqlsrv.so" >> /etc/php/7.0/apache2/php.ini
RUN echo "pdo_sqlsrv.client_buffer_max_kb_size=1000240" >> /etc/php/7.0/apache2/php.ini
RUN echo "sqlsrv.ClientBufferMaxKBSize = 1000240" >> /etc/php/7.0/apache2/php.ini


# load driver sqlsrv
RUN echo "extension=/usr/lib/php/20151012/sqlsrv.so" >> /etc/php/7.0/apache2/php.ini
RUN echo "extension=/usr/lib/php/20151012/pdo_sqlsrv.so" >> /etc/php/7.0/apache2/php.ini
RUN echo "extension=/usr/lib/php/20151012/sqlsrv.so" >> /etc/php/7.0/cli/php.ini
RUN echo "extension=/usr/lib/php/20151012/pdo_sqlsrv.so" >> /etc/php/7.0/cli/php.ini
RUN echo "pdo_sqlsrv.client_buffer_max_kb_size=1000240" >> /etc/php/7.0/cli/php.ini
RUN echo "sqlsrv.ClientBufferMaxKBSize = 1000240" >> /etc/php/7.0/cli/php.ini

# install composer
RUN cd /usr/local/bin && curl -sS https://getcomposer.org/installer | php -- --filename=composer

# Drush
RUN cd /usr/local/src && git clone https://github.com/drush-ops/drush.git \
      && cd /usr/local/src/drush \
      && git checkout -b 8.x origin/8.x \
      && composer install

RUN ln -s /usr/local/src/drush/drush /usr/local/bin/drush

# install ODBC Driver
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql
RUN apt-get install -y mssql-tools
RUN apt-get install -y unixodbc-dev
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN exec bash

# install locales
RUN apt-get install -y locales && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen

# change default to port to 81
RUN sed -i 's/80/81/g' /etc/apache2/ports.conf
#RUN sed -i 's/:80/:81/g' /etc/apache2/sites-available/000-default.conf
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

RUN apt-get install -y nano

# enable mod_rewrite
RUN a2enmod rewrite && service apache2 restart

EXPOSE 81

WORKDIR /var/www/html/

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
